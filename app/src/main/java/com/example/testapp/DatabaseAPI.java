package com.example.testapp;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface DatabaseAPI {

    public static final String BASE_URL = "https://itlearningcenters.com/android/project0206/";

    @FormUrlEncoded
    @POST("regist_shop.php")
    Call<ResponseBody> registerUser(@Field("shop_name") String userNameValue,
                                    @Field("address") String addressValue,
                                    @Field("email") String emailValue,
                                    @Field("phone") String phoneValue,
                                    @Field("username") String usernameValue,
                                    @Field("password") String passwordValue);


    @FormUrlEncoded
    @POST("shop_login.php")
    Call<ResponseBody> login(@Field("username") String userNameValue,
                             @Field("password") String passwordValue);


//    public static final String BASE_URLSHOP = "https://itlearningcenters.com/android/project0309/";
//    @GET("list_shops.php")
//    Call<List<loginresponse>> getShop();


}


package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeApp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_app);
    }
    public void MyShop(View view) {
        Intent intent = new Intent(getApplicationContext(),MyShop.class);
        startActivity(intent);
    }

    public void MyCustomer(View view) {
        Intent intent = new Intent(getApplicationContext(),MyCustomer.class);
        startActivity(intent);
    }

    public void Logout(View view) {
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
    }
}